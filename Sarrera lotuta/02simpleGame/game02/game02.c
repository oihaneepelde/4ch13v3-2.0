#include "game02.h"
#include "imagen.h"
#include "graphics.h"
#include "ebentoak.h"
#include "text.h"
#include "soinua.h"
#include <stdio.h>
#include <windows.h>

#define ONGI_ETORRI_MEZUA "ACHIEVE"
#define JOKOA_SOUND ".\\sound\\132TRANCE_02.wav"
#define JOKOA_PLAYER_IMAGE ".\\img\\invader.bmp"
#define JOKOA_ADINA ".\\img\\Sarrera ofi2.bmp"
#define JOKOA_SOUND_WIN ".\\sound\\BugleCall.wav"
#define JOKOA_SOUND_LOOSE ".\\sound\\terminator.wav" 
#define BUKAERA_SOUND_1 ".\\sound\\128NIGHT_01.wav"
#define BUKAERA_IMAGE ".\\img\\gameOver_1.bmp"

void sarreraMezuaIdatzi();
int aukeratu();
typedef struct S_GURE_GAUZAK
{
    int idIrudi, idMenu, idMapa, idAzalpenak, idKredituak;

}GURE_GAUZAK;

GURE_GAUZAK gureGauzak;
//int  BUKAERA_menua(EGOERA egoera);
int BUKAERA_irudiaBistaratu();


void jokoaAurkeztu(void)
{
    EGOERA  egoera = JOLASTEN;
    int ebentu = 0;
    char str[128];

    POSIZIOA arratoiarenPos;


    sarreraMezuaIdatzi();

    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 236 && arratoiarenPos.x < 761 && arratoiarenPos.y>269 && arratoiarenPos.y < 413)
        {
            aukeratu();
        }
    } while (egoera == JOLASTEN);
    pantailaGarbitu();
    pantailaBerriztu();


}

void sarreraMezuaIdatzi()
{
    pantailaGarbitu();
    gureGauzak.idIrudi = irudiaKargatu(".\\img\\cerebrordenador.bmp");
    irudiaMugitu(gureGauzak.idIrudi, 0, 0);
    irudiakMarraztu();

    pantailaBerriztu();
}

int aukeratu()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKendu(gureGauzak.idIrudi);
    gureGauzak.idMenu = irudiaKargatu(JOKOA_ADINA);
    irudiaMugitu(gureGauzak.idMenu, 0, 0);
    irudiakMarraztu();

    pantailaBerriztu();
    do
    {
        arratoiarenPos = saguarenPosizioa();
        ebentu = ebentuaJasoGertatuBada();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 72 && arratoiarenPos.x < 320 && arratoiarenPos.y>372 && arratoiarenPos.y < 587)
        {
            menuumeak();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 360 && arratoiarenPos.x < 622 && arratoiarenPos.y>372 && arratoiarenPos.y < 587)
        {
            menugazteak();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 627 && arratoiarenPos.x < 881 && arratoiarenPos.y>372 && arratoiarenPos.y < 587)
        {
            menuhelduak();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 881 && arratoiarenPos.x < 931 && arratoiarenPos.y>596 && arratoiarenPos.y < 678)
        {
            jokoaAurkeztu();
        }
    } while (egoera == JOLASTEN);
}

int menuumeak()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKendu(gureGauzak.idIrudi);
    gureGauzak.idAzalpenak = irudiaKargatu(".\\img\\menuhaurrak.bmp");
    irudiaMugitu(gureGauzak.idAzalpenak, 0, 0);
    irudiakMarraztu();
    Subtitulo();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 193 && arratoiarenPos.x < 334 && arratoiarenPos.y>227 && arratoiarenPos.y < 364)
        {
            hasiu();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 492 && arratoiarenPos.x < 643 && arratoiarenPos.y>304 && arratoiarenPos.y < 444)
        {
            azalpenakumeak();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 447 && arratoiarenPos.x < 579 && arratoiarenPos.y>145 && arratoiarenPos.y < 268)
        {
            kredituakumeak();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 833 && arratoiarenPos.x < 933 && arratoiarenPos.y>609 && arratoiarenPos.y < 690)
        {
            aukeratu();
        }
    } while (egoera == JOLASTEN);
}
int menugazteak()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKendu(gureGauzak.idIrudi);
    gureGauzak.idAzalpenak = irudiaKargatu(".\\img\\Hasiera gazteak2.bmp");
    irudiaMugitu(gureGauzak.idAzalpenak, 0, 0);
    irudiakMarraztu();
    Subtitulo();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 407 && arratoiarenPos.x < 546 && arratoiarenPos.y>157 && arratoiarenPos.y < 287)
        {
            hasig();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 766 && arratoiarenPos.x < 890 && arratoiarenPos.y>311 && arratoiarenPos.y < 434)
        {
            azalpenakgazteak();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 67 && arratoiarenPos.x < 198 && arratoiarenPos.y>306 && arratoiarenPos.y < 427)
        {
            kredituakgazteak();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 820 && arratoiarenPos.x < 928 && arratoiarenPos.y>608 && arratoiarenPos.y < 682)
        {
            aukeratu();
        }
    } while (egoera == JOLASTEN);
}
int menuhelduak()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKendu(gureGauzak.idIrudi);
    gureGauzak.idAzalpenak = irudiaKargatu(".\\img\\MenuHelduena.bmp");
    irudiaMugitu(gureGauzak.idAzalpenak, 0, 0);
    irudiakMarraztu();
    Subtitulo();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 370 && arratoiarenPos.x < 562 && arratoiarenPos.y>176 && arratoiarenPos.y < 299)
        {
            hasih();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 674 && arratoiarenPos.x < 892 && arratoiarenPos.y>396 && arratoiarenPos.y < 556)
        {
            azalpenakhelduak();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 48 && arratoiarenPos.x < 238 && arratoiarenPos.y>394 && arratoiarenPos.y < 527)
        {
            kredituakhelduak();
        }
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 845 && arratoiarenPos.x < 941 && arratoiarenPos.y>623 && arratoiarenPos.y < 695)
        {
            aukeratu();
        }
    } while (egoera == JOLASTEN);
}
int hasiu()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKendu(gureGauzak.idIrudi);
    gureGauzak.idAzalpenak = irudiaKargatu(".\\img\\mapaa.bmp");
    irudiaMugitu(gureGauzak.idAzalpenak, 0, 0);
    irudiakMarraztu();
    Subtitulo();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 840 && arratoiarenPos.x < 938 && arratoiarenPos.y>594 && arratoiarenPos.y < 663)
        {
            menuumeak();
        }
    } while (egoera == JOLASTEN);
}
int hasig()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKendu(gureGauzak.idIrudi);
    gureGauzak.idAzalpenak = irudiaKargatu(".\\img\\mapaa.bmp");
    irudiaMugitu(gureGauzak.idAzalpenak, 0, 0);
    irudiakMarraztu();
    Subtitulo();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 840 && arratoiarenPos.x < 938 && arratoiarenPos.y>594 && arratoiarenPos.y < 663)
        {
            menugazteak();
        }
    } while (egoera == JOLASTEN);;
}
int hasih()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKendu(gureGauzak.idIrudi);
    gureGauzak.idAzalpenak = irudiaKargatu(".\\img\\mapaa.bmp");
    irudiaMugitu(gureGauzak.idAzalpenak, 0, 0);
    irudiakMarraztu();
    Subtitulo();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 840 && arratoiarenPos.x < 938 && arratoiarenPos.y>594 && arratoiarenPos.y < 663)
        {
            menuhelduak();
        }
    } while (egoera == JOLASTEN);
}
int azalpenakumeak()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKendu(gureGauzak.idIrudi);
    gureGauzak.idAzalpenak = irudiaKargatu(".\\img\\azalpenakhaurrak2.bmp");
    irudiaMugitu(gureGauzak.idAzalpenak, 0, 0);
    irudiakMarraztu();
    Subtitulo();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 722 && arratoiarenPos.x < 821 && arratoiarenPos.y>594 && arratoiarenPos.y < 663)
        {
            menuumeak();
        }
    } while (egoera == JOLASTEN);
}
int kredituakumeak()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKendu(gureGauzak.idIrudi);
    gureGauzak.idAzalpenak = irudiaKargatu(".\\img\\kredituakhaurrak.bmp");
    irudiaMugitu(gureGauzak.idAzalpenak, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 736 && arratoiarenPos.x < 816 && arratoiarenPos.y>586 && arratoiarenPos.y < 646)
        {
            menuumeak();
        }
    } while (egoera == JOLASTEN);
}
int azalpenakgazteak()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKendu(gureGauzak.idIrudi);
    gureGauzak.idAzalpenak = irudiaKargatu(".\\img\\Nola jokatu.bmp");
    irudiaMugitu(gureGauzak.idAzalpenak, 0, 0);
    irudiakMarraztu();
    Subtitulo();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 792 && arratoiarenPos.x < 929 && arratoiarenPos.y>586 && arratoiarenPos.y < 672)
        {
            menugazteak();
        }
    } while (egoera == JOLASTEN);
}
int kredituakgazteak()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKendu(gureGauzak.idIrudi);
    gureGauzak.idAzalpenak = irudiaKargatu(".\\img\\kredituakgazteak.bmp");
    irudiaMugitu(gureGauzak.idAzalpenak, 0, 0);
    irudiakMarraztu();


    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 812 && arratoiarenPos.x < 925 && arratoiarenPos.y>608 && arratoiarenPos.y < 681)
        {
            menugazteak();
        }
    } while (egoera == JOLASTEN);
}
int azalpenakhelduak()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKendu(gureGauzak.idIrudi);
    gureGauzak.idAzalpenak = irudiaKargatu(".\\img\\NOLAJOKATU.bmp");
    irudiaMugitu(gureGauzak.idAzalpenak, 0, 0);
    irudiakMarraztu();
    Subtitulo();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 812 && arratoiarenPos.x < 925 && arratoiarenPos.y>608 && arratoiarenPos.y < 681)
        {
            menuhelduak();
        }
    } while (egoera == JOLASTEN);
}
int kredituakhelduak()
{
    int ebentu = 0;
    EGOERA  egoera = JOLASTEN;
    POSIZIOA arratoiarenPos;
    pantailaGarbitu();
    irudiaKendu(gureGauzak.idIrudi);
    gureGauzak.idAzalpenak = irudiaKargatu(".\\img\\KREDITUAK.bmp");
    irudiaMugitu(gureGauzak.idAzalpenak, 0, 0);
    irudiakMarraztu();
    pantailaBerriztu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
        arratoiarenPos = saguarenPosizioa();
        if (ebentu == SAGU_BOTOIA_EZKERRA && arratoiarenPos.x > 860 && arratoiarenPos.x < 945 && arratoiarenPos.y>636 && arratoiarenPos.y < 690)
        {
            menuhelduak();
        }
    } while (egoera == JOLASTEN);
}


int  jokoAmaierakoa(EGOERA egoera)
{
    int ebentu = 0, id;
    int idAudioGame;

    loadTheMusic(BUKAERA_SOUND_1);
    if (egoera == IRABAZI)
    {
        idAudioGame = loadSound(JOKOA_SOUND_WIN);
        playSound(idAudioGame);
    }
    else
    {
        idAudioGame = loadSound(JOKOA_SOUND_LOOSE);
        playSound(idAudioGame);
    }
    id = BUKAERA_irudiaBistaratu();
    do
    {
        ebentu = ebentuaJasoGertatuBada();
    } while ((ebentu != TECLA_RETURN) && (ebentu != SAGU_BOTOIA_ESKUMA));
    audioTerminate();
    irudiaKendu(id);
    return (ebentu != TECLA_RETURN) ? 1 : 0;
}

int BUKAERA_irudiaBistaratu()
{
    int id = -1;
    id = irudiaKargatu(BUKAERA_IMAGE);
    irudiaMugitu(id, 200, 200);
    pantailaGarbitu();
    irudiakMarraztu();
    pantailaBerriztu();
    return id;
}
